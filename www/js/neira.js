window.fn = {};

window.fn.loadLink = function (url) {
	window.location.href = url;
};

window.fn.toaster_error = function (jqXHR, exception) {
	if (jqXHR.status === 0) {
		// ons.notification.toast('Terjadi kesalahan. Coba cek koneksi anda.', {
		// 	buttonLabel: 'Dismiss',
		// 	timeout: 1500
		// });
		ons.notification.alert({
			title: "Terjadi kesalahan",
			message: "Coba cek koneksi Anda. Tidak terhubung dengan Server."
		}).then(function(name) {
			window.location.reload()
		});

		// fn.showDialog('dialog__url_server')
	}else if (jqXHR.status == 404) {
		ons.notification.toast('URL server tidak ditemukan.', {
			buttonLabel: 'Dismiss',
			timeout: 1500
		})
		// fn.showDialog('dialog__url_server')
		console.error('Requested url not found. [404]');
	} else if (jqXHR.status == 401) {
		ons.notification.toast('Cek Auth', {
			buttonLabel: 'Dismiss',
			timeout: 1500
		}).then(function () {
			fn.pushPage({
				'id': 'login.html',
				'title': 'Login'
			}, 'lift-md')
		})
	} else if (jqXHR.status == 500) {
		ons.notification.toast('Terjadi Kesalahan Server', {
			buttonLabel: 'Dismiss',
			timeout: 1500
		});
	} else if (exception === 'parsererror') {
		console.error('Requested JSON parse failed.');
	} else if (exception == 'timeout') {
		console.error('Time out error.');
		// console.log("asdhkasd")
		// alert('Terjadi kesalahan. Coba cek koneksi anda.')
		ons.notification.toast('Gateway Timeout', {
			buttonLabel: 'Dismiss',
			timeout: 1500
		});
	} else if (exception === 'abort') {
		console.error('Ajax request aborted.');
	} else {
		console.error('Uncaught Error: ' + jqXHR.responseText);
	}
}